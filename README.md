# `bf-picolisp`

A Brainfuck-to-PicoLisp compiler, written in PicoLisp.

## Usage
Directly execute the Brainfuck program `hello.bf`:
```
picolisp bf.l -x hello.bf
```

Compile `hello.bf` to `hello.l`, then execute it as PicoLisp:
```
picolisp bf.l -o hello.l -c hello.bf
picolisp bf.l hello.l
```

Compile `hello.bf` to PicoLisp and print the compiled code to `stdout`:
```
picolisp bf.l -c hello.bf
```

## Overview
Brainfuck is a Turing-complete language with only eight instructions. Memory is represented by an infinite series of cells, one of which is active. Each cell can hold a single number. The instructions modify the active cell, change which cell is active, read and write between the active cell and I/O, and loop. For more information, see [the entry for Brainfuck on Esolang](https://esolangs.org/wiki/Brainfuck).

The compiler has two parts: the runtime, which consists of functions that emulate the Brainfuck memory cells, and the parser, which reads characters and generates calls to the runtime functions.

### Runtime
Each Brainfuck instruction corresponds to a single PicoLisp function.
* `>` is represented by `(bf-right)`
* `<` is represented by `(bf-left)`
* sequences of `+` and `-` (which act as a single add to the current cell) are represented by `(bf-add n)`, where `n` is the effective addend for the current cell
* `.` is represented by `(bf-out)`
* `,` is represented by `(bf-in)`
* `[...]` is represented by `(bf-loop ...)`

### Parser
The function `parse` reads a single logical instruction and produces function calls as above. It uses the helper function `snarf-adds`, which consumes consecutive `+` and `-` characters and returns a single `(bf-add n)` instruction.

To read an entire Brainfuck program, `parse-prog` repeatedly calls `parse` until the end of the file. These calls produce a list of functions. Before returning, `parse-prog` collects the functions into a single `(prog ...)` block (which is a single instruction), and finally appends `(bye)`, which will exit PicoLisp when run.

### Compiler
To parse and run a file immediately, the function `x` reads a single command-line option, treats it as a filename (or `stdin` if none is supplied), parses it using `parse-prog`, then runs the result.

The function `o` sets the global variable `outfile` to the following command-line option for use in `c`.

For compiling a program without running it, the function `c` reads a single command-line option, treats it as a filename (or `stdin` if none is supplied), parses it using `parse-prog`, then writes the result to `outfile` if set, or `stdout` otherwise.

Note that, since the runtime is written in the same language as the compiled output, there is no need to duplicate the runtime functions in the output. Also, since PicoLisp scripts are executed by running command-line arguments as functions in the active environment (_e.g._, `picolisp bf.l -x` loads the file `bf.l` and then runs the function `x`), the file `bf.l` need not have any effect when run by itself. Therefore `bf.l` can set up the environment both for executing a Brainfuck program _and_ for compiling one!

## Notes

PicoLisp is an unusually apt language for this compiler because the process of parsing Brainfuck into logical structures (turning `[-]` into a loop, for instance) is exactly the same as "compiling" the Brainfuck directly into PicoLisp.

This program is an atypical Brainfuck compiler for two reasons:
1. The memory cells are not represented by a flat preallocated array, but rather by two stacks: one stack for the cells to the left of the active cell, and one for the cells to the right. If, for instance, a `>` instruction is encountered when the right stack is empty, the program acts as if a `0` were popped.
2. Each cell is represented by the PicoLisp numeric primitive, which is an arbitrary-size integer.

Because of the first reason, this compiler is a _correct_ Brainfuck compiler in the sense that, if there were no memory restrictions, a Brainfuck program could use as many of its infinite memory cells as necessary. Obviously all computers have finite memory, but in principle this implementation is not limited, unlike, say, `unsigned char cells[65536];` would be in C.

Because of the second reason, this program acts differently from other Brainfuck interpreters, almost all of which use bounded integers. There is no universal Brainfuck specification, but it seems to be undefined behavior when an addition or subtraction instruction would give a cell too large a value or a negative value. One result of this feature is that `[-]`, which in bounded implementations always terminates and leaves the active cell `0`, only does so in `bf-picolisp` for nonnegative cells. For negative cells, the simple program `[-]` does not terminate.

I suspect that the only requirements for Brainfuck to be Turing complete are that there be at least two cell values (_i.e._, one bit per cell) and that there be an infinite number of cells. More precisely, there must be enough cell values to represent the output language (in this case, UTF-8), and the cells must be infinite on one side. However, I haven't actually found a formal proof of Brainfuck's Turing completeness, so I can't confirm this postulate right now.

I find this implementation of Brainfuck particularly attractive because of how it runs on the PicoLisp virtual machine. According to Alexander Burger [in the PicoLisp Reference](http://software-lab.de/doc/ref.html), "The PicoLisp virtual machine is both simpler and more powerful than most current (hardware) processors." Next he describes _cells_, the fundamental building block of PicoLisp data structures. Unlike modern processors, which have a hard (if enormous) limit to the amount of memory that can be referenced, any PicoLisp cell can refer to any other. Of course, by running on a processor with finite memory, PicoLisp programs are naturally constrained by the environment, but the fact remains that there is no _language-defined_ bound on memory in PicoLisp. The theoretical limitations of PicoLisp are in the computer that it runs on, not the language itself.

Similarly, Brainfuck has an unbounded number of cells, and that property makes it Turing complete. Even though it can only run on a computer with finite memory, there is no _language-defined_ bound on memory, in precisely the same way as PicoLisp. I find that symmetry very pleasing. This implementation of Brainfuck, if it were run on a PicoLisp with unlimited memory, would have the correct (unbounded) number of cells. Put another way, given that PicoLisp is Turing complete, this implementation of Brainfuck is also truly Turing complete.